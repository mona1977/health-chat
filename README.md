#DEVELOPER - SURENDRA GUPTA

## Health HEALTH Exchange Program

I select FHIR to create a chat system to query health records in a natural way

## Use cases for the health chat system

1) You can list all patients and see the blood pressure history of a identified patient. 
2) you can type something like 
        a) list all patients
                or
        b) get all patients

Once you have an patient id, you can list the blood pressure history of this one.

### Prerequisites

- 6GB Memory (or more if you can)
- 2CPU (or more if you can)



Running in linux and MacOS
